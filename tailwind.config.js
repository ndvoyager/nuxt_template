/** @type {import('tailwindcss').Config} */
export default {
	content: [`./pages/**/*.vue`],
	theme: {
		extend: {
			fontFamily: {
				sans: ['Roboto', 'sans'],
				serif: ['FastSerif', 'sans-serif'],
			},
		},
	},
	plugins: [],
};
