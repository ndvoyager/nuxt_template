import withNuxt from './.nuxt/eslint.config.mjs';

export default withNuxt(
	{

		files: ['**/*.ts'],
		ignores: ['*.config/*', 'package.json', 'package-lock.json'],
		rules: {
			'max-len': ['warn', { code: 90, tabWidth: 2 }],
		},
	},
);
