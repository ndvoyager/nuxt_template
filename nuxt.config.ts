// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	devtools: { enabled: true },
	css: [
		'@/assets/css/main.css',
	],
	modules: ['@nuxtjs/tailwindcss', '@nuxt/eslint'],
	tailwindcss: {
		cssPath: '~/assets/css/tailwind.css',
		exposeConfig: true,
	},
	eslint: {
		checker: true,
		config: {
			stylistic: {
				indent: 'tab',
				semi: true,
			},
		},
	},
	app: {
		head: {
			title: 'Default',
			titleTemplate: '%s | App Template',
		},
	},

	$development: {
		app: {
			head: {
				title: 'Dev',
			},
		},
	},

	$production: {
		app: {
			head: {
				title: 'Prod',
			},
		},
	},

	$test: {
		app: {
			head: {
				title: 'Test',
			},
		},
	},
});
