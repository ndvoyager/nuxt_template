# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```
Change to Production while in Dev mode:

```bash
# npm
NODE_ENV=production npm run dev

# pnpm
NODE_ENV=production pnpm dev

```

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```
## Directory Structure



# ***.nuxt***
 - Nuxt uses the .nuxt directory in development to generate your Vue application.

# ***.output***
 - Nuxt creates the .output directory when building your application for production.
 
 - This directory should be added to your .gitignore file to avoid pushing the build output to your repository.

 - Use this directory to deploy your Nuxt application to production.

# ***assets***
 - The assets directory is used to add all the websites assets that the build tool will process

# ***components***
 - The components/ directory is where you put all your Vue components.

 - Nuxt automatically imports any components in this directory along with components that are registered by any modules you may be using.

# ***composables***
 - Use the composables/ directory to auto-import your Vue composables into your application. i.e. functions/methods

# ***content***
  - Use the content/ directory to create a file-based CMS for your application.

# ***layouts***
 - Nuxt provides a layouts framework to extract common UI patterns into reusable layouts.

# ***middleware***
 - Nuxt provides middleware to run code before navigating to a particular route.

# ***modules***
 - Use the modules/ directory to automatically register local modules within your application.

 - It is a good place to place any local modules you develop while building your application.

# ***pages***
 - Nuxt provides file-based routing to create routes within your web application.

# ***plugins***
 - Nuxt has a plugins system to use Vue plugins and more at the creation of your Vue application.

# ***public***
 - The public/ directory is used to serve your websites static assets.

# ***server***
 - The server/ directory is used to register API and server handlers to your application.

 - Nuxt automatically scans files inside these directories to register API and server handlers with Hot Module Replacement HMR support.

# ***utils***
 - Use the utils/ directory to auto-import your utility functions throughout your application.

# ***.nuxtignore***
 - The .nuxtignore file lets Nuxt ignore files in your projects root directory during the build phase.

# ***app.vue***
 - The app.vue file is the main component of your Nuxt application.

# ***app.config.ts***
 - Expose reactive configuration within your application with the App Config file.

# ***error.vue***
 - The error.vue file is the error page in your Nuxt application.

# ***nuxt.config.ts***
 - Nuxt can be easily configured with a single nuxt.config file.

# ***tsconfig.json***
 - Nuxt generates a .nuxt/tsconfig.json file with sensible defaults and your aliases.
```
Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
